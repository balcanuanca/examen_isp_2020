package ex1;

public class S1 {
	

}

class Car extends Vehicle{
	
	private String color;
	private Wheel[] wheels = new Wheel[4];
	private Windshield ws= new Windshield();
	private CarMaker thisCarMaker = new CarMaker();
	
	
	public void start()
	{
		
	}
	
	public void stop() {
		
	}
	
	public void go()
	{
		
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Wheel[] getWheels() {
		return wheels;
	}

	public void setWheels(Wheel[] wheels) {
		this.wheels = wheels;
	}

	public Windshield getWs() {
		return ws;
	}

	public void setWs(Windshield ws) {
		this.ws = ws;
	}

	public CarMaker getThisCarMaker() {
		return thisCarMaker;
	}

	public void setThisCarMaker(CarMaker thisCarMaker) {
		this.thisCarMaker = thisCarMaker;
	}
}

class User{
	
}

class Vehicle{
	
}

class Windshield{
	
}

class CarMaker{
	
}

class Wheel{
	
}