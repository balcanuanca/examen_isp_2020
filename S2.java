package ex2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class S2 {

	public  static void main(String[] args)
	{
		JFrame frame = new JFrame();
		frame.setLayout(null);
		frame.setSize(300, 300);
		
		JTextField textF = new JTextField();
		textF.setBounds(100, 100, 100, 20);
		
		
		JButton button = new JButton("Print");
		button.setBounds(100, 120, 80, 30);
		button.addActionListener(new ActionListener(){  
		    public void actionPerformed(ActionEvent e){  
		            System.out.println(textF.getText()); 
		    }  
		    });
		
		frame.add(textF);
		frame.add(button);
		frame.setVisible(true);
	}
}
